//são os 3 circulos que são coloridos de azul no passo a passo
var step1 = $("#step1");
var step2 = $("#step2");
var step3 = $("#step3");

// define os IDs dos passos do financiamento
var sectionDadosPessoais = $("#DadosPessoais");
var sectionSimulacao = $("#Simulacao");
var sectionFinanciamento = $("#Financiamento");
var sectionVoucher = $("#Voucher");

//são os botões laranjas de confirmação dos passos, respectivamente: Próximo (Sessão de Dados Pessoais), Enviar (Sessão de Simulação) e Continuar (Sessão de Financiamento)
var btnStep2 = $("#btnStep2");
var btnStep3 = $("#btnStep3");
var btnVoucher = $("#btnVoucher");

//define os IDs dos campos de digitação
var campoEmail = $("#campoEmail");
var campoCelular = $("#campoCelular");
var campoDataNascimento = $("#campoDataNascimento");
var campoCpf = $("#campoCpf");
var campoEntrada = $("#entrada");
var radio = $("#radioSim");

var formDados = $("#formDados")[0];

//coloca display hidden nas sessões Simulação, Financiamento e Voucher; desmarca os radios (para evitar que o usuário volte a página com o radio de "Sem habilitação" marcado); e colore o circulo da sessão Dados Pessoais, para indicar que está ativa
function displayCampos(){
    sectionSimulacao.addClass("hidden");
    sectionFinanciamento.addClass("hidden");
    sectionVoucher.addClass("hidden");
    $("#radioSim").prop("checked", false);
    $("#radioNao").prop("checked", false);
    step1.css("background-color", "#2196f3");
}

//mostra modal de retorno ao site, pois o usuário não possui habilitação, logo não pode financiar um veículo
function modalSemCNH(){
    $("#modalCNH").modal('show');
}

//define as mascaras de telefone, data de nascimento, cpf, e valor de entrada
var SPMaskBehavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
spOptions = {
    onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
    }
};

$('#campoCelular').mask(SPMaskBehavior, spOptions);
$('#campoDataNascimento').mask('00/00/0000');
$('#campoCpf').mask('000.000.000-00', {reverse: true});

$('#entrada').mask('#.##0,00', {reverse: true});

//valida se os campos estão preenchidos e passa para a próxima etapa
btnStep2.on("click", function() {
    if (formDados.checkValidity() == true) {
        event.preventDefault();
        sectionDadosPessoais.addClass("hidden");
        sectionSimulacao.removeClass("hidden");
        sectionSimulacao.addClass("show");
        step1.css("background-color", "#979797");
        step2.css("background-color", "#2196f3");
    }else{
        btnStep2.click();
    }
});

//valida se os campos estão preenchidos e passa para a próxima etapa
btnStep3.on("click", function() {
    event.preventDefault();
    sectionSimulacao.removeClass("show");
    sectionSimulacao.addClass("hidden");
    sectionFinanciamento.removeClass("hidden");
    sectionFinanciamento.addClass("show");
    step2.css("background-color", "#979797");
    step3.css("background-color", "#2196f3");
});

function alterarEntrada(){
    event.preventDefault();
    sectionSimulacao.removeClass("hidden");
    sectionSimulacao.addClass("show");
    step1.css("background-color", "#979797");
    step2.css("background-color", "#2196f3");
    step3.css("background-color", "#979797");
    sectionFinanciamento.addClass("hidden");
    sectionFinanciamento.removeClass("show");
}

btnVoucher.on("click", function() {
    event.preventDefault();
    sectionFinanciamento.removeClass("show");
    sectionFinanciamento.addClass("hidden");
    sectionVoucher.removeClass("hidden");
    sectionVoucher.addClass("show");
    step3.css("background-color", "#979797");
});